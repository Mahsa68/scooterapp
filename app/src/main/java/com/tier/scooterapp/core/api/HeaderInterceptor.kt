package com.tier.scooterapp.core.api

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Mahsa on 2022.03.19
 *
 * This class is responsible for adding headers to request.
 *
 */

class HeaderInterceptor(private val secretKey: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        request().newBuilder().run {
            addHeader("Content-Type", "application/json")
            addHeader("secret-key", secretKey)
            proceed(build())
        }
    }
}