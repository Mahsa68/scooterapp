package com.tier.scooterapp.core.di

import com.tier.scooterapp.data.scooter.ScooterRepository
import com.tier.scooterapp.data.scooter.remote.IScooterRemoteDataSource
import com.tier.scooterapp.data.scooter.remote.datasource.ScooterRemoteDataSource
import com.tier.scooterapp.data.scooter.remote.datasource.ScooterService
import com.tier.scooterapp.domain.scooter.IScooterRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

/**
 * Created by Mahsa on 2022.03.19
 */

val repositoriesModule = module {
    // Retrofit
    factory { get<Retrofit>(named(RETROFIT)).create(ScooterService::class.java) }

    factory<IScooterRemoteDataSource> { ScooterRemoteDataSource(get()) }
    factory<IScooterRepository> { ScooterRepository(get()) }
}