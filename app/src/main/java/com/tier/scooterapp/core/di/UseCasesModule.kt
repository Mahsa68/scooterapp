package com.tier.scooterapp.core.di

import com.tier.scooterapp.domain.scooter.usecases.GetScootersUseCase
import org.koin.dsl.module

/**
 * Created by Mahsa on 2022.03.19
 */

val useCasesModule = module {
    factory { GetScootersUseCase(get()) }
}