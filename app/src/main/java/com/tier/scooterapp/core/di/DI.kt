package com.tier.scooterapp.core.di

val koinModules = listOf(
    networkModule,
    repositoriesModule,
    useCasesModule,
    viewModelsModule
)