package com.tier.scooterapp.core.di

import com.tier.scooterapp.presentation.ui.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Mahsa on 2022.03.19
 */

val viewModelsModule = module {
    viewModel { MainViewModel(get()) }
}