package com.tier.scooterapp.core

import com.tier.scooterapp.BuildConfig

/**
 * Created by Mahsa on 2022.03.19
 */
object Const {
    const val baseUrl = BuildConfig.BASE_URL
    const val secretKey = BuildConfig.SECRET_KEY
}