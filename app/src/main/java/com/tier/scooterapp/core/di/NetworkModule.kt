package com.tier.scooterapp.core.di

import com.tier.scooterapp.core.Const
import com.tier.scooterapp.core.api.HeaderInterceptor
import com.tier.scooterapp.core.api.MyCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Mahsa on 2022.03.19
 */

const val LOGGING_INTERCEPTOR = "LOGGING_INTERCEPTOR"
const val HEADER_INTERCEPTOR = "HEADER_INTERCEPTOR"
const val OK_HTTP = "OK_HTTP"
const val RETROFIT = "RETROFIT"

val networkModule = module {

    factory(named(LOGGING_INTERCEPTOR)) {
        HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.HEADERS)
            .setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    factory(named(HEADER_INTERCEPTOR)) {
        HeaderInterceptor(Const.secretKey)
    }

    factory(named(OK_HTTP)) {
        OkHttpClient.Builder()
            .readTimeout(30 * 1000, TimeUnit.MILLISECONDS)
            .writeTimeout(30 * 1000, TimeUnit.MILLISECONDS)
            .connectTimeout(30 * 1000, TimeUnit.MILLISECONDS)
            .addInterceptor(get<HttpLoggingInterceptor>(named(LOGGING_INTERCEPTOR)))
            .addInterceptor(get<HeaderInterceptor>(named(HEADER_INTERCEPTOR)))
            .build()
    }

    single(named(RETROFIT)) {
        Retrofit.Builder()
            .client(get(named(OK_HTTP)))
            .baseUrl(Const.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(MyCallAdapterFactory())
            .build()
    }
}