package com.tier.scooterapp.data.scooter.remote.datasource

import com.tier.scooterapp.core.api.Result
import com.tier.scooterapp.data.scooter.remote.model.ScooterResponse
import retrofit2.http.GET

/**
 * Created by Mahsa on 2022.03.19
 *
 * This service is responsible for fetching data from remote with Retrofit
 *
 */

interface ScooterService {

    @GET("b/5fa8ff8dbd01877eecdb898f")
    suspend fun getScooters(): Result<List<ScooterResponse>?>
}