package com.tier.scooterapp.data.scooter.remote

import com.tier.scooterapp.core.api.Result
import com.tier.scooterapp.domain.scooter.entities.ScooterEntity

/**
 * Created by Mahsa on 2022.03.19
 *
 * This data source is responsible for fetching data from remote
 *
 */

interface IScooterRemoteDataSource {
    suspend fun getScooters(): Result<List<ScooterEntity>?>
}