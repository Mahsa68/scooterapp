package com.tier.scooterapp.data.scooter.remote.model

import com.tier.scooterapp.domain.scooter.entities.ScooterEntity

/**
 * Created by Mahsa on 2022.03.19
 */
data class ScooterResponse(
    val name: String,
    val lat: Double,
    val lng: Double,
    val description: String?
)

fun ScooterResponse.mapToDomain() = ScooterEntity(
    this.name,
    this.lat,
    this.lng,
    this.description
)
