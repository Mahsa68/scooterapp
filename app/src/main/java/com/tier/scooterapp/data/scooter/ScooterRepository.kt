package com.tier.scooterapp.data.scooter

import com.tier.scooterapp.data.scooter.remote.IScooterRemoteDataSource
import com.tier.scooterapp.domain.scooter.IScooterRepository

/**
 * Created by Mahsa on 2022.03.19
 */
class ScooterRepository(private val remoteDataSource: IScooterRemoteDataSource) :
    IScooterRepository {

    override suspend fun getScooters() = remoteDataSource.getScooters()
}