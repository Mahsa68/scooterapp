package com.tier.scooterapp.data.scooter.remote.datasource

import com.tier.scooterapp.core.api.map
import com.tier.scooterapp.data.scooter.remote.IScooterRemoteDataSource
import com.tier.scooterapp.data.scooter.remote.model.mapToDomain

/**
 * Created by Mahsa on 2022.03.19
 */
class ScooterRemoteDataSource(private val scooterService: ScooterService) :
    IScooterRemoteDataSource {

    override suspend fun getScooters() =
        scooterService.getScooters().map {
            it?.map { scooterResponse -> scooterResponse.mapToDomain() }
        }
}

