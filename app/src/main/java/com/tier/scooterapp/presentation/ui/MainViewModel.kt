package com.tier.scooterapp.presentation.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tier.scooterapp.core.api.subscribeBy
import com.tier.scooterapp.core.ui.State
import com.tier.scooterapp.domain.scooter.usecases.GetScootersUseCase
import com.tier.scooterapp.presentation.model.Scooter
import com.tier.scooterapp.presentation.model.mapToView
import kotlinx.coroutines.launch

class MainViewModel(
    private val getScootersUseCase: GetScootersUseCase,
) : ViewModel() {

    val scootersLiveData: MutableLiveData<State<List<Scooter>>> = MutableLiveData()

    fun getScooters() {
        scootersLiveData.value = State.Loading

        viewModelScope.launch {
            getScootersUseCase().subscribeBy(
                onSuccess = { scootersEntity ->
                    scootersLiveData.value = State.Success(
                        scootersEntity?.map { scooterEntity -> scooterEntity.mapToView() }
                    )
                },
                onError = {
                    scootersLiveData.value = State.Error(it.message ?: "")
                }
            )
        }
    }
}