package com.tier.scooterapp.presentation.model

import com.tier.scooterapp.domain.scooter.entities.ScooterEntity

/**
 * Created by Mahsa on 2022.03.19
 */
data class Scooter(
    val name: String,
    val lat: Double,
    val lng: Double,
    val description: String?
)

fun ScooterEntity.mapToView() = Scooter(
    this.name,
    this.lat,
    this.lng,
    this.description
)
