package com.tier.scooterapp

import android.app.Application
import android.content.Context
import com.tier.scooterapp.core.di.koinModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Mahsa on 2022.03.19
 */

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        // Start koin DI
        startKon(this)
    }

    /**
     * start koin modules
     */
    private fun startKon(context: Context) {
        startKoin {
            androidContext(context)
            modules(koinModules)
        }
    }
}