package com.tier.scooterapp.domain.scooter.usecases

import com.tier.scooterapp.domain.scooter.IScooterRepository

/**
 * Created by Mahsa on 2022.03.19
 *
 * This use case is responsible for getting scooters data
 *
 */

class GetScootersUseCase(private val scooterRepository: IScooterRepository) {
    suspend operator fun invoke() = scooterRepository.getScooters()
}