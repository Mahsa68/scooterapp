package com.tier.scooterapp.domain.scooter

import com.tier.scooterapp.core.api.Result
import com.tier.scooterapp.domain.scooter.entities.ScooterEntity

/**
 * Created by Mahsa on 2022.03.19
 */
interface IScooterRepository {
    suspend fun getScooters(): Result<List<ScooterEntity>?>
}