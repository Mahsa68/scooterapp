package com.tier.scooterapp.domain.scooter.entities

/**
 * Created by Mahsa on 2022.03.19
 */
data class ScooterEntity(
    val name: String,
    val lat: Double,
    val lng: Double,
    val description: String?
)
